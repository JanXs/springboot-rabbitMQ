package com.kzj.kzj_rabbitmq.consumer.StrategyClass;

import com.kzj.kzj_rabbitmq.consumer.Strategy;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @Auther: cjw
 * @Date: 2018/8/2 10:31
 * @Description: 具体策略类
 */
@Component("B")
public class B implements Strategy {


    /**
     *
     * @param params 接口所需参数
     */
    @Override
    public void doJob(Map<String, Object> params) {
        System.out.println("用B方法处理");
    }
}
