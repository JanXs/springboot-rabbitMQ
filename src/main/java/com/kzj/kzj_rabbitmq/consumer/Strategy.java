package com.kzj.kzj_rabbitmq.consumer;

import java.util.Map;

/**
 * @Auther: cjw
 * @Date: 2018/8/1 16:40
 * @Description: 策略接口
 */
public interface Strategy {

    public void doJob(Map<String, Object> params) throws Exception;

}
