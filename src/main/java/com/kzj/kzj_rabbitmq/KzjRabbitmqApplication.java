package com.kzj.kzj_rabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KzjRabbitmqApplication {

    public static void main(String[] args) {
        SpringApplication.run(KzjRabbitmqApplication.class, args);
    }
}
