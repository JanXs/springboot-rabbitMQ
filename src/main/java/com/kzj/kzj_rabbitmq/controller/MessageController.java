package com.kzj.kzj_rabbitmq.controller;

import com.alibaba.fastjson.JSON;
import com.kzj.kzj_rabbitmq.common.MessagePojo;
import com.kzj.kzj_rabbitmq.common.QueueEnum;
import com.kzj.kzj_rabbitmq.provider.MessageProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther: cjw
 * @Date: 2018/6/28 16:47
 * @Description:
 */
@RestController
public class MessageController {

    @Autowired
    private MessageProvider provider;

    Logger logger = LoggerFactory.getLogger(MessageController.class);

    @RequestMapping(value="/send_message",produces = "text/json;charset=UTF-8")
    @ResponseBody
    public String send_message(MessagePojo pojo){
        try {
           provider.sendMessage(pojo);
            return JSON.toJSONString(pojo);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
